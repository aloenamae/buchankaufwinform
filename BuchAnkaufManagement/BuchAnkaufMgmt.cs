﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BuchAnkaufManagement
{
    public partial class BuchAnkauf_Mgmt : Form
    {
        public BuchAnkauf_Mgmt()
        {
            InitializeComponent();
            BuchAnkaufMgmt();
        }

        private static MongoCollection<transactionsEntity> collection;
        
        

        public static void SetCollection()
        {

            MongoClient Client = new MongoClient("mongodb://client144:client144devnetworks@144.76.166.207/BuchAnkauf");
            MongoServer Server = Client.GetServer();
            MongoDatabase Database = Server.GetDatabase("BuchAnkauf");
            collection = Database.GetCollection<transactionsEntity>("transactions");
        }

        
        public void BuchAnkaufMgmt()
        {
            SetCollection();
   
            var result = collection.FindAll();
      
            foreach (var books in result)
            {
                   dgUsedbooks.Rows.Add(
                        string.Join("|",books.eans),
                        books.username,
                        books.paid,
                        books.shipped,
                        books.timestamp
                        );    
            }
        }


        private void btnpaid_Click(object sender, EventArgs e)
        {
            var query = Query<transactionsEntity>.EQ(x => x.paid, false);
            var update = Update<transactionsEntity>.Set(x => x.paid, true);
            collection.Update(query, update, UpdateFlags.Multi);
        }

        private void btnship_Click(object sender, EventArgs e)
        {
            var query = Query<transactionsEntity>.EQ(x => x.shipped, false);
            var update = Update<transactionsEntity>.Set(x => x.shipped, true);
            collection.Update(query, update, UpdateFlags.Multi);

        }


        
    }
}
