﻿namespace BuchAnkaufManagement
{
    partial class BuchAnkauf_Mgmt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgUsedbooks = new System.Windows.Forms.DataGridView();
            this.eans = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shipped = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.timestamp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnpaid = new System.Windows.Forms.Button();
            this.btnship = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgUsedbooks)).BeginInit();
            this.SuspendLayout();
            // 
            // dgUsedbooks
            // 
            this.dgUsedbooks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgUsedbooks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.eans,
            this.username,
            this.paid,
            this.shipped,
            this.timestamp});
            this.dgUsedbooks.Location = new System.Drawing.Point(37, 79);
            this.dgUsedbooks.Name = "dgUsedbooks";
            this.dgUsedbooks.Size = new System.Drawing.Size(557, 297);
            this.dgUsedbooks.TabIndex = 0;
            // 
            // eans
            // 
            this.eans.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.eans.HeaderText = "EANS";
            this.eans.Name = "eans";
            this.eans.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.eans.Width = 61;
            // 
            // username
            // 
            this.username.HeaderText = "Username";
            this.username.Name = "username";
            // 
            // paid
            // 
            this.paid.HeaderText = "Paid";
            this.paid.Name = "paid";
            // 
            // shipped
            // 
            this.shipped.HeaderText = "Shipped";
            this.shipped.Name = "shipped";
            // 
            // timestamp
            // 
            this.timestamp.HeaderText = "Date";
            this.timestamp.Name = "timestamp";
            // 
            // btnpaid
            // 
            this.btnpaid.Location = new System.Drawing.Point(37, 12);
            this.btnpaid.Name = "btnpaid";
            this.btnpaid.Size = new System.Drawing.Size(113, 23);
            this.btnpaid.TabIndex = 1;
            this.btnpaid.Text = "Update Paid";
            this.btnpaid.UseVisualStyleBackColor = true;
            this.btnpaid.Click += new System.EventHandler(this.btnpaid_Click);
            // 
            // btnship
            // 
            this.btnship.Location = new System.Drawing.Point(37, 41);
            this.btnship.Name = "btnship";
            this.btnship.Size = new System.Drawing.Size(113, 23);
            this.btnship.TabIndex = 2;
            this.btnship.Text = "Update Shipped";
            this.btnship.UseVisualStyleBackColor = true;
            this.btnship.Click += new System.EventHandler(this.btnship_Click);
            // 
            // BuchAnkauf_Mgmt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(634, 421);
            this.Controls.Add(this.btnship);
            this.Controls.Add(this.btnpaid);
            this.Controls.Add(this.dgUsedbooks);
            this.Name = "BuchAnkauf_Mgmt";
            this.Text = "BuchAnkauf Management";
            ((System.ComponentModel.ISupportInitialize)(this.dgUsedbooks)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgUsedbooks;
        private System.Windows.Forms.Button btnpaid;
        private System.Windows.Forms.Button btnship;
        private System.Windows.Forms.DataGridViewTextBoxColumn eans;
        private System.Windows.Forms.DataGridViewTextBoxColumn username;
        private System.Windows.Forms.DataGridViewTextBoxColumn paid;
        private System.Windows.Forms.DataGridViewTextBoxColumn shipped;
        private System.Windows.Forms.DataGridViewTextBoxColumn timestamp;
    }
}

