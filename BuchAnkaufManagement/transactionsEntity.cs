﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BuchAnkaufManagement
{
    public class transactionsEntity
    {
        public Object id { get; set; }
        public List<string> eans { get; set; }
        public string username { get; set; }
        public bool shipped { get; set; }
        public bool paid { get; set; }
        public DateTime timestamp { get; set; }
    }
}
